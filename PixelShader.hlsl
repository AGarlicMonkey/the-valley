#define MAX_STEPS 100
#define MAX_DIST 1000.0f
#define SURF_DIST 0.001f

//Define a mod as hlsl's works slightly differently
#define goodmod(x, y) ((x) - (y) * floor((x) / (y)))

cbuffer ScreenSizeBuffer : register(b1){
	float2 screenSize;
}

cbuffer TimeBuffer : register(b2){
	float time;
}

//Operations
float smoothUnion(float distA, float distB, float amount){
	float h = clamp(0.5f + 0.5f * (distB - distA) / amount, 0.0f, 1.0f);
	return lerp(distB, distA, h) - amount * h * (1.0f - h);
}

float smoothSubtraction(float distA, float distB, float amount){
	float h = clamp(0.5f - 0.5f * (distA + distB) / amount, 0.0f, 1.0f);
	return lerp(distB, -distA, h);
}

float smoothIntersection(float distA, float distB, float amount){
	float h = clamp(0.5f - 0.5f * (distB - distA) / amount, 0.0f, 1.0f);
	return lerp(distB, distA, h) + amount * h * (1.0f - h);
}

//Distance Functions
float planeDistance(float3 pos){
	return pos.y + 1.0f;	
}

float sphereDistance(float3 pos, float3 p, float radius){
	return length(p - pos) - radius;
}

float capsuleDistance(float3 pos, float3 pa, float3 pb, float radius){
	float3 pointA = pos - pa;
	float3 pointB = pb - pa;
	float h = clamp(dot(pointA, pointB) / dot(pointB, pointB), 0.0f, 1.0f);
	return length(pointA - pointB * h) - radius;
}

float stalkThingDistance(float3 pos, float3 p){
	float height = 3.0f;
	
	float3 hp = float3(p.x, p.y + height, p.z);
	float3 ebp = float3(hp.x , hp.y, hp.z - 0.15f);
	
	float subBallRadius = sin(time);
	
	float stalk = capsuleDistance(pos, p, hp, 0.1f);
	float mainBall = sphereDistance(pos, hp, 0.58f);
	float subBall = sphereDistance(pos, ebp, abs(subBallRadius * 0.55f));
	
	float ball = smoothSubtraction(subBall, mainBall, 0.0001f);
	
	return smoothUnion(stalk, ball, 0.4f);
}

//Displacements
float3 bend(float3 pos, float k){
	float c = cos(k * pos.x);
	float s = sin(k * pos.x);
	float2x2 m = float2x2(c, -s, s, c);
	return float3(mul(m, pos.xy), pos.z);
}


float3 rotateY(float3 pos, float k){
    float c = cos(k);
    float s = sin(k);
    float2x2 m = float2x2(c, -s, s, c);
	float2 rot = mul(m, pos.xz);
	return float3(rot.x, pos.y, rot.y);
}

//Scene
struct SceneInfo{
	float distance;
	float3 colour;
	int steps;
};
SceneInfo getSceneInfo(float3 pos){
	pos = bend(pos, -0.005f);
	
	pos.z += time * 2.5f;
	pos.x += time * 1.5f;
	
	float k = 1.0f;
	float d = sin(k * pos.x) * sin(k * pos.y) * sin(k * pos.z);
	float plane = planeDistance(pos);
	pos.x -= time * 1.5f;
	
	pos.xz = goodmod(pos.xz, 6.0f) - float2(3.0f, 3.0f);
	
	float stalkThing = stalkThingDistance(pos, float3(0.0f, -2.0f, 0.0f));
	
	SceneInfo info;
	info.distance = min(stalkThing, plane + d);	
	info.colour = float3(stalkThing, plane, 1.0f);
	
	return info;
}

SceneInfo march(float3 origin, float3 direction){
	SceneInfo info;

    for(int i = 0; i < MAX_STEPS; ++i){
		float3 pos = origin + direction * info.distance;
    	SceneInfo currentInfo = getSceneInfo(pos);

		info.distance += currentInfo.distance;
		info.colour = currentInfo.colour;
		info.steps = i;

		if(info.distance > MAX_DIST || currentInfo.distance < SURF_DIST){
			break;
		}
	}

    return info;
}

//Lighting
float3 getNormal(float3 pos){
	float distance = getSceneInfo(pos).distance;
	float2 epsilon = float2(0.01f, 0.0f);

	float3 normal = distance - float3(	getSceneInfo(pos - epsilon.xyy).distance, 
										getSceneInfo(pos - epsilon.yxy).distance, 
										getSceneInfo(pos - epsilon.yyx).distance);

	return normalize(normal);
}

float calcDiffuseDirectionalLight(float3 pos){
	float3 dirToLight = normalize(float3(0.0f, 1.0f, -1.0f));
	
	float3 posNormal = getNormal(pos);
	
	float lightStrength = clamp(dot(posNormal, dirToLight), 0.0f, 1.0f);

	float distToLight = march(pos + posNormal * SURF_DIST * 2.0f, dirToLight).distance;
	if(distToLight < MAX_DIST){
		lightStrength *= 0.1f;	
	}
	
	return lightStrength;
}

float4 main(float2 tex : TexCoord) : SV_Target{
	float2 pixelPos = float2(tex.x * screenSize.x, tex.y * screenSize.y);
	float2 uv = (pixelPos - 0.5f * screenSize) / screenSize.y;

	//float3 rayOrigin = float3(sin(time) * 2.0f, cos(time) * 0.5f + 0.5f, time * 5.0f);
	float3 rayOrigin = float3(0.0f, 2.0f, 5.0f);
    float3 rayDirection = normalize(float3(uv.x, uv.y, 1.0f));
	
	SceneInfo info = march(rayOrigin, rayDirection);
	
	float3 col = float3(0.0f, 0.3f, 0.7f);
	
	if(info.distance < MAX_DIST){
		float3 currentPos = rayOrigin + rayDirection * info.distance;
		float diffLight = calcDiffuseDirectionalLight(currentPos);
		
		float spct = 1.0f - ((float)info.steps / (float)MAX_STEPS);
		
		col = info.colour * diffLight * spct;
	}

	return float4(col, 1.0f);
}